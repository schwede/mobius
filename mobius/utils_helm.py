#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Mobius - utils for HELM strings
#

import copy
import json
import os
import random
import re
import tqdm
import yaml
from collections import defaultdict
from importlib import util
from itertools import combinations

import networkx as nx
import numpy as np
import pandas as pd
import torch
from rdkit import Chem
from rdkit import RDLogger
from rdkit.Chem import rdmolops

import numpy as np
import matplotlib.pyplot as plt


def path_module(module_name):
    """
    Given a module name, return the path of the directory where the module is located.
    Returns None if the module does not exist.

    Parameters
    ----------
    module_name : str
        Name of the module.

    Returns
    -------
    path : str or None
        Path of the directory where the module is located, or None if the module does not exist.

    """
    specs = util.find_spec(module_name)
    if specs is not None:
        return specs.submodule_search_locations[0]
    return None


def build_helm_string(complex_polymer, connections=None):
    """
    Build a HELM string from a dictionary of polymers and a list of connections.

    Parameters
    ----------
    complex_polymer : dict
        A dictionary of simple polymers, where keys are the simple polymer types 
        and values are lists of monomer symbols.
    connections : List, default : None
        A list of connections, where each connection is represented 
        as a tuple with six elements: (start_polymer, start_monomer, start_attachment, 
        end_polymer, end_monomer, end_attachment).

    Returns
    -------
    str
        The generated polymer in HELM format.

    """
    tmp = []

    for pid, simple_polymer in complex_polymer.items():
        if 'CHEM' in pid:
            simple_polymer_str = '%s{[%s]}' % (pid, simple_polymer)
        elif 'PEPTIDE' in pid or 'RNA' in pid:
            simple_polymer_str = '%s{%s}' % (pid, '.'.join([m if len(m) == 1 else '[%s]' % m for m in simple_polymer]))
        else:
            raise ValueError(f'{pid} is an invalid "Simple Polymer" type. Only PEPTIDE, RNA and CHEM type are allowed.')
            
        tmp.append(simple_polymer_str)

    complex_polymer_str = '|'.join(tmp)

    if connections is not None:
        connections_str = '|'.join(['%s,%s,%d:%s-%d:%s' % (c[0], c[1], c[2], c[3], c[4], c[5]) for c in connections])
    else:
        connections_str = ''

    polymer = '%s$%s$$$V2.0' % (complex_polymer_str, connections_str)
    
    return polymer


def parse_helm(polymer):
    """
    Parses a HELM string and returns the relevant information.

    Parameters
    ----------
    polymer (str)
        A polymer in HELM format.

    Returns
    -------
    complex_polymer : dict
        A dictionary containing the simple polymer IDs (pid) as keys and simple polymer as values.
    connections : numpy.ndarray
        An array with dtype [('SourcePolymerID', 'U20'), ('TargetPolymerID', 'U20'),\
                             ('SourceMonomerPosition', 'i4'), ('SourceAttachment', 'U2'),\
                             ('TargetMonomerPosition', 'i4'), ('TargetAttachment', 'U2')].
        Each row represents a connection between two monomers in the complex polymer.
    hydrogen_bonds : str
        A string containing information about any hydrogen bonds in the complex polymer.
    attributes : str
        A string containing any additional attributes related to the complex polymer.

    Raises
    ------
    ValueError
        If the HELM string is invalid.
        If contains an invalid Simple Polymer type.
        If a Simple Polymer was already defined.
        If contains an invalid connection.

    """
    dtype = [('SourcePolymerID', 'U20'), ('TargetPolymerID', 'U20'),
             ('SourceMonomerPosition', 'i4'), ('SourceAttachment', 'U2'),
             ('TargetMonomerPosition', 'i4'), ('TargetAttachment', 'U2')]

    components = polymer.split('$')
    
    try:
        complex_polymer_str, connections, hydrogen_bonds, attributes = '$'.join(components[0:-4]), components[-4], components[-3], components[-2]
    except ValueError:
        raise ValueError(f'Invalid for HELM string {polymer}')

    # Process polymer
    complex_polymer = {}

    for simple_polymer_str in re.split(r'(?<=\})\|(?=\w+\{)', complex_polymer_str):        
        pid = simple_polymer_str.split('{')[0]

        if 'CHEM' in pid:
            # If the monomer starts with [ and ends with ], then we must have a CXSMILES in between
            if simple_polymer_str[len(pid) + 1:].startswith('[') and simple_polymer_str[:-1].endswith(']'):
                simple_polymer = [simple_polymer_str[len(pid) + 2:-2]]
            else:
                simple_polymer = [simple_polymer_str[len(pid) + 1: -1]]

            if '.' in simple_polymer[0]:
                raise ValueError('CHEM Simple Polymer cannot contains more than one monomer.')
        elif 'PEPTIDE' in pid or 'RNA' in pid:
            simple_polymer = [monomer.strip("[]") for monomer in simple_polymer_str[len(pid) + 1:-1].split('.')]
        else:
            raise ValueError(f'{pid} is an invalid "Simple Polymer" type. Only PEPTIDE, RNA and CHEM type are allowed.')

        if pid in complex_polymer:
            raise ValueError(f'Simple polymer {pid} is already defined.')
        
        complex_polymer[pid] = simple_polymer

    # Process connections
    data = []
    if connections:
        for connection in connections.split('|'):
            # Look for "PolymerID,PolymerID,X:X-X:X" pattern in connections
            if not re.search("\w+,\w+,\w+:\w+-\w+:\w+", connection):
                raise ValueError(f'Invalid connections "{connection}" for HELM string "{polymer}"')
            
            source_id, target_id, con = connection.split(',')
            source_position, source_attachment = con.split('-')[0].split(':')
            target_position, target_attachment = con.split('-')[1].split(':')
            data.append((source_id, target_id,
                         source_position, source_attachment,
                         target_position, target_attachment))

    connections = np.array(data, dtype=dtype)

    return complex_polymer, connections, hydrogen_bonds, attributes


def MolFromHELM(polymers, HELM_extra_library_filename=None):
    """
    Generate a list of RDKit molecules from HELM strings.

    Parameters
    ----------
    polymers : str or List or tuple or numpy.ndarray
        The polymer in HELM format to convert to RDKit molecules.
    HELM_extra_library_filename : str, default : None
        The path to a HELM Library file containing extra monomers.
        Extra monomers will be added to the internal monomers library. 
        Internal monomers can be overriden by providing a monomer with
        the same MonomerID.

    Returns
    -------
    List
        A list of RDKit molecules.

    Raises
    ------
    KeyError
        If a monomer is unknown.
    ValueError
        If monomer SMILES is invalid.
        If capping SMILES is invalid.
        If attachement point is not defined for a monomer.

    """
    rdkit_polymers = []

    if not isinstance(polymers, (list, tuple, np.ndarray)):
            polymers = [polymers]

    # Read HELM Core Library
    d = path_module("mobius")
    HELMCoreLibrary_filename = os.path.join(d, "data/monomer_library.json")

    with open(HELMCoreLibrary_filename) as f:
        data = json.load(f)

    # Re-organize monomer data in a dictionary for faster access
    HELMCoreLibrary = {monomer['MonomerID']: monomer for monomer in data}

    # Read HELM Extra Library
    if HELM_extra_library_filename is not None:
        with open(HELM_extra_library_filename) as f:
            data = json.load(f)

        HELMExtraLibrary = {monomer['MonomerID']: monomer for monomer in data}
        # Add new monomers to the internal monomers library or
        # override existing monomers with the same MonomerID
        HELMCoreLibrary.update(HELMExtraLibrary)

    RDLogger.DisableLog('rdApp.warning')

    for polymer in polymers:
        molecules_to_zip = []

        complex_polymer, connections, _, _ = parse_helm(polymer)

        #print(complex_polymer)
        #print(connections)

        for pid, simple_polymer in complex_polymer.items():
            number_monomers = len(simple_polymer)

            for i, monomer_symbol in enumerate(simple_polymer):
                non_canonical_points = {}
                canonical_points = {}

                if 'PEPTIDE' in pid or 'RNA' in pid:
                    try:
                        monomer_data = HELMCoreLibrary[monomer_symbol]
                    except KeyError:
                        raise KeyError(f'Monomer {monomer_symbol} unknown.')
    
                    # Read SMILES string
                    monomer = Chem.MolFromSmiles(monomer_data['MonomerSmiles'])

                    if monomer is None:
                        raise ValueError(f'Invalid monomer SMILES {monomer_data["MonomerSmiles"]} for {monomer_symbol} in {pid}.')

                    #print(monomer_symbol, (i + 1), HELMCoreLibrary[monomer_symbol]['smiles'])
                else:
                    monomer = Chem.MolFromSmiles(monomer_symbol)

                    if monomer is None:
                        raise ValueError(f'Invalid monomer SMILES {monomer_symbol} in {pid}.')

                # Get all the non-canonical attachment points
                if connections.size > 0:
                    c_ids = np.where((connections['SourceMonomerPosition'] == (i + 1)) | (connections['TargetMonomerPosition'] == (i + 1)))[0]

                    if c_ids.size > 0:
                        for connection in connections[c_ids]:
                            if connection['SourcePolymerID'] == pid and connection['SourceMonomerPosition'] == (i + 1):
                                non_canonical_points['_%s' % connection['SourceAttachment']] = '_'.join(['%s' % s for s in connection])
                            elif connection['TargetPolymerID'] == pid and connection['TargetMonomerPosition'] == (i + 1):
                                non_canonical_points['_%s' % connection['TargetAttachment']] = '_'.join(['%s' % s for s in connection])

                #print('Non-canonical attachments: ', non_canonical_points)

                # Only monomers in PEPTIDE and RNA have canonical connections
                if 'PEPTIDE' in pid or 'RNA' in pid:
                    # Get all the canonical attachment points (not involved in a non-canonical attachment)
                    for r in monomer_data['Attachments']:
                        label = '_%s' % r['AttachmentLabel']
    
                        if label not in non_canonical_points:
                            monomer_cap = None
                            monomer_cap_smiles = None
    
                            if label == '_R1':
                                if i == 0:
                                    # If it is the first monomer and R1 is not involved in a non-canonical connections, then add cap
                                    key = '%s_%d_%s' % (pid, (i + 1), r['AttachmentLabel'])
                                    monomer_cap_smiles = r['CapGroupSmiles']
                                else:
                                    # Look at R2 of the previous monomer and check if not involved in a non-canonical connections
                                    r2_1 = np.where((connections['SourcePolymerID'] == pid) & (connections['SourceMonomerPosition'] == i) & (connections['SourceAttachment'] == 'R2'))[0]
                                    r2_2 = np.where((connections['TargetPolymerID'] == pid) & (connections['TargetMonomerPosition'] == i) & (connections['TargetAttachment'] == 'R2'))[0]
    
                                    if r2_1.size or r2_2.size:
                                        # R2_previous monomer involved in non-canonical connection 
                                        key = '%s_%d_%s' % (pid, (i + 1), r['AttachmentLabel'])
                                        monomer_cap_smiles = r['CapGroupSmiles']
                                        #print('R2 of previous monomer involved in a non-canonical attachments!!')
                                    else:
                                        # Canonical connection between R2_previous and R1_current monomers
                                        key = '%s_%d_%d' % (pid, i, (i + 1))
                            elif label == '_R2':
                                if i == number_monomers - 1:
                                    # If it is the last monomer and R2 is not involved in non-canonical connections, then add cap
                                    key = '%s_%d_%s' % (pid, (i + 1), r['AttachmentLabel'])
                                    monomer_cap_smiles = r['CapGroupSmiles']
                                else:
                                    # Look at R1 of the next monomer and check if not involved in a non-canonical connections
                                    r1_1 = np.where((connections['SourcePolymerID'] == pid) & (connections['SourceMonomerPosition'] == (i + 2)) & (connections['SourceAttachment'] == 'R1'))[0]
                                    r1_2 = np.where((connections['TargetPolymerID'] == pid) & (connections['TargetMonomerPosition'] == (i + 2)) & (connections['TargetAttachment'] == 'R1'))[0]
    
                                    if r1_1.size or r1_2.size:
                                        # R1_next monomer involved in non-canonical connection 
                                        key = '%s_%d_%s' % (pid, (i + 1), r['AttachmentLabel'])
                                        monomer_cap_smiles = r['CapGroupSmiles']
                                        #print('R1 of next monomer involved in a non-canonical attachments!!')
                                    else:
                                        # Canonical connection between R2_current and R2_next monomers
                                        key = '%s_%d_%d' % (pid, (i + 1), (i + 2))
                            else:
                                # For every R3, R4,... not involved in a non-canonical connections
                                key = '%s_%d_%s' % (pid, (i + 1), r['AttachmentLabel'])
                                monomer_cap_smiles = r['CapGroupSmiles']
    
                            canonical_points[label] = (key, monomer_cap_smiles)

                #print('Canonical attachments: ', canonical_points)

                # Set all the attachment points
                for atm in monomer.GetAtoms():
                    if atm.HasProp('atomLabel') and atm.GetProp('atomLabel').startswith('_R'):
                        label = atm.GetProp('atomLabel')

                        if label in non_canonical_points:
                            # Set the non-canonical attachment points in the monomer
                            hashed_key = abs(hash(non_canonical_points[label])) % (10 ** 8)
                            atm.SetAtomMapNum(hashed_key)
                        elif label in canonical_points:
                            # Set the canonical attachment points in the monomer and monomer cap
                            hashed_key = abs(hash(canonical_points[label][0])) % (10 ** 8)
                            atm.SetAtomMapNum(hashed_key)

                            # Set canonical attachment point in monomer cap
                            if canonical_points[label][1] is not None:
                                cap_smiles = canonical_points[label][1]

                                cap = Chem.MolFromSmiles(cap_smiles)

                                if cap is None:
                                    raise ValueError(f'Invalid capping SMILES ({cap_smiles}) for {monomer_symbol} in {pid}.')

                                for cap_atm in cap.GetAtoms():
                                    if cap_atm.HasProp('atomLabel') and cap_atm.GetProp('atomLabel') == label:
                                        #print('-- Monomer cap on: %s - %s (%d)' % (label, cap_smiles, hashed_key))
                                        cap_atm.SetAtomMapNum(hashed_key)
                                # ... and add monomer cap to polymer
                                molecules_to_zip.append(cap)
                        else:
                            msg_error = f'Attachment point for {label} in monomer {monomer_symbol} located in polymer {pid} is missing '
                            msg_error += f'({polymer}).'
                            raise ValueError(msg_error)

                molecules_to_zip.append(monomer)

                #print('')

        with Chem.RWMol() as rw_polymer:
            [rw_polymer.InsertMol(molecule) for molecule in molecules_to_zip]

        # Bop-it, Twist-it, Pull-it and Zip-it!
        rdkit_polymer = Chem.molzip(rw_polymer)

        # Clean mol and remove dummy H atoms
        Chem.SanitizeMol(rdkit_polymer)
        params = Chem.RemoveHsParameters()
        params.removeDegreeZero = True
        rdkit_polymer = Chem.RemoveHs(rdkit_polymer, params)

        rdkit_polymers.append(rdkit_polymer)

    RDLogger.EnableLog('rdApp.warning')

    return rdkit_polymers


def remove_dummy_atoms(mol):
    dummy_mol = Chem.MolFromSmarts('[#0]')
    atoms_to_remove = [a[0] for a in mol.GetSubstructMatches(dummy_mol)]
    atoms_to_remove.sort(reverse=True)
    
    mol = Chem.EditableMol(mol)
    
    for atom in atoms_to_remove:
        mol.RemoveAtom(atom)
    
    mol = mol.GetMol()

    return mol


def generate_combinations(lst):
    result = []
    for i in range(1, len(lst) + 1):
        result.extend([list(comb) for comb in combinations(lst, i)])
    return result


def prepare_monomer_library(library):
    idx = 0

    already_added = []
    prepared_library = {}
    
    for monomer_name, monomer in library.items():
        attachments = {}
        already_added = []
        
        mol_monomer = Chem.MolFromSmiles(monomer['MonomerSmiles'])
    
        # Prepare attachments
        for attachment in monomer['Attachments']:
            mol_cap = Chem.MolFromSmiles(attachment['CapGroupSmiles'])
    
            # Keep cap only if it has heavy atoms
            if mol_cap.GetNumHeavyAtoms():
                for atom in mol_cap.GetAtoms():
                    if atom.HasProp('atomLabel') and atom.GetProp('atomLabel').startswith('_R'):
                        label = atom.GetProp('atomLabel')
                        atom.SetAtomMapNum(int(label[2:]))
        
                attachments[attachment['AttachmentLabel']] = [attachment['CapGroupSmiles'], mol_cap]
    
        # Prepare monomer
        for atom in mol_monomer.GetAtoms():
            if atom.HasProp('atomLabel') and atom.GetProp('atomLabel').startswith('_R'):
                label = atom.GetProp('atomLabel')
                atom.SetAtomMapNum(int(label[2:]))
    
                if not label in already_added:
                    # Transfer R label to the connected atom
                    neighbor_atom = atom.GetNeighbors()[0]
                    neighbor_atom.SetProp('atomLabel', label)
                    neighbor_atom_id = neighbor_atom.GetIdx()
        
                    if label[1:] in attachments:
                        attachments[label[1:]].append(neighbor_atom_id)
    
                    already_added.append(label)
    
        # Add monomer with no attachment at all
        prepared_mol_monomer = remove_dummy_atoms(Chem.Mol(mol_monomer))
        prepared_smiles_monomer = Chem.MolToSmiles(prepared_mol_monomer)
    
        prepared_library[idx] = {
            'ID': monomer_name,
            'mol': prepared_mol_monomer,
            'type': monomer['PolymerType'],
            'attachments': [a['AttachmentLabel'] for a in monomer['Attachments']]
        }
    
        idx += 1
    
        # If there is only one attachment point we won't add the cap 
        # ... otherwise we cannot use it as a monomer. obviously...
        if len(monomer['Attachments']) == 1:
            continue
    
        # Add monomer with combination of attachments
        attachment_combinations = generate_combinations(attachments.keys())
    
        for attachment_combination in attachment_combinations:
            molecules_to_zip = [Chem.Mol(mol_monomer)]
    
            # Since we are going to add the cap, we can remove the R label
            for attachment_label in attachment_combination:
                atom_idx = attachments[attachment_label][-1]
                molecules_to_zip[0].GetAtomWithIdx(atom_idx).ClearProp('atomLabel')
                molecules_to_zip.extend([Chem.Mol(attachments[attachment_label][-2])])
            
            with Chem.RWMol() as rw_mol_monomer:
                [rw_mol_monomer.InsertMol(molecule) for molecule in molecules_to_zip]
    
            # Zip all the attachments on the monomer
            prepared_mol_monomer = Chem.molzip(rw_mol_monomer)
    
            # ... and clean up the mess
            Chem.SanitizeMol(prepared_mol_monomer)
            params = Chem.RemoveHsParameters()
            params.removeDegreeZero = True
            prepared_mol_monomer = Chem.RemoveHs(prepared_mol_monomer, params)
            prepared_mol_monomer = remove_dummy_atoms(prepared_mol_monomer)
    
            prepared_smiles_monomer = Chem.MolToSmiles(prepared_mol_monomer)
    
            if not prepared_smiles_monomer in already_added:
                already_added.append(prepared_smiles_monomer)
        
                prepared_library[idx] = {
                    'ID': monomer_name,
                    'mol': prepared_mol_monomer,
                    'type': monomer['PolymerType'],
                    'attachments': [a['AttachmentLabel'] for a in monomer['Attachments'] if not a['AttachmentLabel'] in attachment_combination]
                }
        
                idx += 1
            else:
                continue
        
    return prepared_library


def map_monomers_to_molecule(mol, prepared_library):
    idx = 0

    monomer_vectors = []
    monomer_infos = {}
    
    mol_n_atom = mol.GetNumHeavyAtoms()
    
    for monomer_idx, monomer in prepared_library.items():
        matches = mol.GetSubstructMatches(monomer['mol'], useChirality=True)
    
        for match in matches:
            valid = True
    
            monomer_info = {'ID': monomer['ID'],
                            'attachments': {},
                            'type': monomer['type']}
    
            # For each atom that matches in the peptide...
            # The atom order in match is the same as in the monomer
            # so the monomer_atom_idx and mol_atom_idx represent
            # the same atom, one in the monomer, the other in the
            # peptide molecule
            for monomer_atom_idx, mol_atom_idx in enumerate(match):
                # Get equivalent atom in peptide and monomer
                mol_atom = mol.GetAtomWithIdx(mol_atom_idx)
                monomer_atom = monomer['mol'].GetAtomWithIdx(monomer_atom_idx)
                
                # Get atom indices of all the neighbors in peptide
                mol_neighbor_atom_idxs = set([mol_neighbor_atom.GetIdx() for mol_neighbor_atom in mol_atom.GetNeighbors()])
                # Keep only the ones that were not matched
                mol_neighbor_atom_idxs = mol_neighbor_atom_idxs.difference(match)
    
                # Get monomer properties
                monomer_atom_props = monomer_atom.GetPropsAsDict()
    
                if 'atomLabel' in monomer_atom_props:
                    monomer_info['attachments'][mol_atom_idx] = monomer_atom_props['atomLabel']
    
                # If the monomer atom has the atomLabel in its properties,
                # that's an attachment point! Otherwise, it means we won't be 
                # able to connect this monomer to the rest of the peptide 
                # since it is not an attachment point!
                if mol_neighbor_atom_idxs and not 'atomLabel' in monomer_atom_props:
                    valid = False
    
            if valid:
                v = np.zeros(shape=(mol_n_atom)).astype(int)
                v[np.array(match)] = 1
        
                monomer_vectors.append(v)
                monomer_infos[idx] = monomer_info
        
                idx += 1
    
    monomer_vectors = np.asarray(monomer_vectors)

    return monomer_vectors, monomer_infos


def map_fragments_to_molecule(mol, missing_atom_ids, monomer_infos):
    # Get all the different fragments corresponding to the missing atoms ids
    fragments, fragment_atom_ids = get_connected_substructures(mol, missing_atom_ids)
    
    smiles_fragments = [Chem.MolToSmiles(fragment) for fragment in fragments]

    fragment_vectors = []
    fragment_infos = {}

    mol_n_atom = mol.GetNumHeavyAtoms()

    for i in range(len(fragments)):
        dummy_atoms_to_add = []
        attachments = {}

        j = 1
        
        for fragment_atom_id, mol_atom_id in enumerate(fragment_atom_ids[i]):
            neighbor_mol_atoms = mol.GetAtomWithIdx(mol_atom_id).GetNeighbors()
            neighbor_mol_atoms_ids = [neighbor_mol_atom.GetIdx() for neighbor_mol_atom in neighbor_mol_atoms]
            neighbor_mol_atoms_ids = list(set(neighbor_mol_atoms_ids).difference(fragment_atom_ids[i]))

            # Here we do not have to check again that the atom is connected to has an attachement point.
            # Because we already did that before when we mapped the monomers on the molecule.
            # Only monomers that have attachment points are kept.

            if neighbor_mol_atoms_ids:
                dummy_atoms_to_add.append(fragment_atom_id)
                attachments[mol_atom_id] = f'_R{2 + j}'
                j += 1

        # Add dummy atom in reverse order, otherwise we have issue during 
        # the insertion. It shifts all the atom ids downstream if you start
        # with the atom with the lowest atom id
        dummy_atoms_to_add = np.sort(dummy_atoms_to_add)[::-1]

        with Chem.RWMol(fragments[i]) as mw:
            for dummy_atom_to_add in dummy_atoms_to_add:
                dummy = Chem.Atom(0)
                dummy_atom_idx = mw.AddAtom(dummy)
                mw.AddBond(int(dummy_atom_to_add), dummy_atom_idx, Chem.BondType.SINGLE)

        # Generate canonical smiles
        # Because of that the atom order will now will be different from the information
        # in the monomer_infos, but this is fine. It is not necessary for building the graph.
        # Just don't be suprised if the smiles and attachment points are not corresponding 
        # anymore.
        fragments[i] = Chem.MolFromSmiles(Chem.MolToSmiles(mw))
        smiles_fragments[i] = Chem.MolToSmiles(fragments[i]).replace('*', '[*]')

        # Add now the chemaxon informations about the attachment points in the smiles
        j = 1
        cx_features = []

        for atom in fragments[i].GetAtoms():
            if atom.GetAtomicNum() == 0:
                cx_features.append(f'_R{2 + j}')
                j += 1
            else:
                cx_features.append('')

        cx_features = ';'.join(cx_features)

        cx_features = f'|${cx_features}$|'
        smiles_fragments[i] = f'{smiles_fragments[i]} {cx_features}'

        # Generate vector for that fragment
        v = np.zeros(shape=(mol_n_atom)).astype(int)
        v[np.array(fragment_atom_ids[i])] = 1
        fragment_vectors.append(v)

        # Save all the needed information for that fragment for building the graph
        fragment_info = {'ID': smiles_fragments[i], 'attachments': attachments, 'type': 'CHEMICAL'}
        fragment_infos[np.max(list(monomer_infos.keys())) + 1 + i] = fragment_info

    fragment_vectors = np.asarray(fragment_vectors)

    return fragment_vectors, fragment_infos


def check_continuity(vector):
    ones_indices = np.where(vector == 1)[0]  # Get indices where vec == 1
    
    if len(ones_indices) == 0:  # No 1s in the vector
        return False
    
    # Check if the indices form a continuous sequence
    return np.all(np.diff(ones_indices) == 1)


def remove_non_continuous_monomers(monomer_vectors, monomer_infos):
    continuous = []

    for i in range(0, len(monomer_vectors)):
        if check_continuity(monomer_vectors[i]):
            continuous.append(i)
    
    continuous = np.asarray(continuous)
    
    monomer_vectors = monomer_vectors[continuous]
    monomer_infos = {i: monomer_infos[c] for i, c in enumerate(continuous)}

    return monomer_vectors, monomer_infos


def check_overlapping(A, B):
    size_A = np.sum(A)
    size_B = np.sum(B)

    if size_A >= size_B:
        if np.all((A & B) == B):
            return True, 1
    elif size_A < size_B:
        if np.all((B & A) == A):
            return True, 0

    return False, None


def remove_enclosed_monomers(monomer_vectors, monomer_infos):
    overlapped_monomer_idxs = []

    for i in range(0, len(monomer_vectors)):
        for j in range(i + 1, len(monomer_vectors)):
            fully_overlaped, which_one = check_overlapping(monomer_vectors[i], monomer_vectors[j])
    
            if fully_overlaped:
                overlapped_monomer_idxs.append([i, j][which_one])
    
    overlapped_monomer_idxs = np.sort(overlapped_monomer_idxs)
    overlapped_monomer_idxs = np.unique(overlapped_monomer_idxs)

    select_nonoverlapping = np.ones(shape=monomer_vectors.shape[0]).astype(bool)
    select_nonoverlapping[overlapped_monomer_idxs] = False

    monomer_vectors = monomer_vectors[select_nonoverlapping]
    monomer_infos = {i: monomer_infos[idx] for i, idx in enumerate(np.argwhere(select_nonoverlapping).flatten())}

    return monomer_vectors, monomer_infos


def check_partial_overlap(vectors):
    return np.where(np.sum(vectors, axis=0) > 1)[0]


def is_peptide_fully_covered(vectors):
    return np.all(np.sum(vectors, axis=0) == 1)


def check_missing_coverage(vectors):
    return np.where(np.sum(vectors, axis=0) == 0)[0]


def get_connected_substructures(mol, atom_ids):
    """
    Given an RDKit molecule and a list of atom IDs, returns a list of lists where each sublist
    represents a set of atom IDs that are continuously connected.
    """
    atom_ids = np.asarray(atom_ids)

    # Create a subgraph containing only the selected atoms
    submol = Chem.PathToSubmol(mol, atom_ids.tolist())
    
    # Get connected fragments (returns tuple of atom index tuples)
    fragment_atom_ids = []
    fragments = rdmolops.GetMolFrags(submol, asMols=True, sanitizeFrags=False, fragsMolAtomMapping=fragment_atom_ids)

    fragments = list(fragments)
    
    return fragments, fragment_atom_ids


def build_graph(mol, monomer_infos):
    G = nx.DiGraph()

    G.add_nodes_from([(f'{m_name}:{m_info["ID"]}', {'name': m_info['ID'], 'attachments': sorted(m_info['attachments'].values()), 'type': m_info['type']}) for m_name, m_info in monomer_infos.items()])
    
    # For each monomer
    for resid_i in monomer_infos.keys():
        node_label_i = f'{resid_i}:{monomer_infos[resid_i]["ID"]}'
        type_i = monomer_infos[resid_i]['type']
    
        # ... and for each attachment point in that monomer
        for mol_atom_i_idx, attachment_type_i in monomer_infos[resid_i]['attachments'].items():
            found_pairing = False
    
            # Get all the atoms connected to that attachment point
            neighbors_mol_atom_ids_i = [neighbor_atom.GetIdx() for neighbor_atom in mol.GetAtomWithIdx(mol_atom_i_idx).GetNeighbors()]
    
            # ... and search to which monomer on of these atom correpond
            for resid_j in monomer_infos.keys():
                node_label_j = f'{resid_j}:{monomer_infos[resid_j]["ID"]}'
                mol_atom_j_ids = list(monomer_infos[resid_j]['attachments'].keys())
                is_in_there = list(set(neighbors_mol_atom_ids_i).intersection(mol_atom_j_ids))
    
                if is_in_there:
                    attachment_type_j = monomer_infos[resid_j]['attachments'][is_in_there[0]]
    
                    found_pairing = True
    
                    if attachment_type_i == '_R1' and attachment_type_j == '_R2':
                        # Ignore edges going from R1 to R2 (cter to nter). We want to
                        # have directed edges going from R2 to R1 (nter to cter). But all 
                        # the others will have then a directed edge in both direction. Which
                        # will help us later on differentiate the canonical from the non
                        # canonical bonds between monomers
                        pass
                    else:
                        G.add_edge(node_label_i, node_label_j, 
                                   attachment_type=[attachment_type_i, attachment_type_j], 
                                   attachment_order=[resid_i, resid_j])

    return G


def dfs(graph, node, path, visited, end_nodes, paths, ignored_edges=None):
    """ Depth-First Search (DFS) to explore all paths recursively, avoiding cyclic edges."""
    path.append(node)
    visited.add(node)

    if ignored_edges is None:
        ignored_edges = []

    if node in end_nodes:
        paths.append(path.copy())
    else:
        for neighbor in graph.neighbors(node):
            if (node, neighbor) not in ignored_edges and (neighbor, node) not in ignored_edges and neighbor not in visited:
                dfs(graph, neighbor, path, visited, end_nodes, paths, ignored_edges)
    
    path.pop()
    visited.remove(node)


def find_all_non_canonical_bonds(graph):
    """Identifies pairs of nodes that have mutual edges (point to each other)."""
    mutual_edges = set()

    for u, v in graph.edges():
        if graph.has_edge(v, u) and (v, u) not in mutual_edges:
            mutual_edges.add((u, v))

    mutual_edges = list(mutual_edges)

    return mutual_edges


def find_all_canonical_structures(graph, mutual_edges):
    """
    Finds all paths in a directed graph from start nodes to end nodes.
    Avoids cyclic structures (mutual edges between two nodes).
    """
    all_paths = []

    g = copy.deepcopy(graph)

    # We remove all the cyclic structures from the graph, it makes things easier...
    for mutual_edge in mutual_edges:
        u, v = mutual_edge
        g.remove_edge(u, v)
        g.remove_edge(v, u)

    # Now remove all the nodes that are not connected to anything anymore.
    # This is happening when you have fragments in your polymer
    isolated_nodes = list(nx.isolates(g))

    for isolated_node in isolated_nodes:
        g.remove_node(isolated_node)

    # Identify start and end nodes, nodes with no incoming and outgoing edges respectively
    start_nodes = [n for n, d in g.in_degree() if d == 0]
    end_nodes = [n for n, d in g.out_degree() if d == 0]

    if len(start_nodes) != len(end_nodes):
        msg_error = 'The number of start nodes and end nodes must be equal: \n'
        msg_error += f'Start nodes :  {start_nodes} \n'
        msg_error += f'End nodes  :  {end_nodes} \n'
        raise ValueError(msg_error)

    # Identify all the paths between the start and end nodes
    for start in start_nodes:
        dfs(g, start, [], set(), end_nodes, all_paths)

    # Sort paths from longest to shortest
    all_paths.sort(key=len, reverse=True)
    
    return all_paths
    

def find_all_head_to_tail_structures_and_bonds(graph, mutual_edges):
    extra_cyclic_edges = []
    all_paths = []

    g = copy.deepcopy(graph)

    # We remove all the cyclic structures from the graph, it makes things easier...
    for mutual_edge in mutual_edges:
        u, v = mutual_edge
        g.remove_edge(u, v)
        g.remove_edge(v, u)

    # Now remove all the nodes that are not connected to anything anymore.
    # This is happening when you have fragments in your polymer
    isolated_nodes = list(nx.isolates(g))

    for isolated_node in isolated_nodes:
        g.remove_node(isolated_node)

    # Identify all the strongly connected graphs
    for nodes in nx.strongly_connected_components(g):
        nodes = list(nodes)

        # If there is only one strongly connected node, then it is
        # not a head-to-tail structure
        if len(nodes) == 1:
            continue

        # Takes arbitrarily the first node
        start_node = nodes[0]

        # The end node will be the one that points to start_node
        end_node = list(g.predecessors(start_node))[0]

        # Add connection
        extra_cyclic_edges.append((end_node, start_node))
            
        # Identify all the paths between the start and end nodes
        dfs(g, start_node, [], set(), end_node, all_paths)

    # Sort paths from longest to shortest
    all_paths.sort(key=len, reverse=True)

    return all_paths, extra_cyclic_edges


def build_complex_polymer_and_connections(G, simple_polymer_paths, connection_pairs, monomer_infos):
    # Gather all the informations to build the HELM string
    dtype = [('SourcePolymerID', 'U20'), ('TargetPolymerID', 'U20'),
     ('SourceMonomerPosition', 'i4'), ('SourceAttachment', 'U2'),
     ('TargetMonomerPosition', 'i4'), ('TargetAttachment', 'U2')]

    complex_polymer = {}
    connections = []
    
    i = 1
    
    # Add first the CHEMICAL simple polymers
    for monomer_idx, monomer_info in monomer_infos.items():
        if monomer_info['type'] == 'CHEMICAL':
            complex_polymer[f'CHEM{i}'] = f'{monomer_info["ID"]}'
            i += 1
    
    # Then we add the PEPTIDE simple polymer
    for i, simple_polymer_path in enumerate(simple_polymer_paths):
        simple_polymer = [node.split(':')[1] for node in simple_polymer_path]
        complex_polymer[f'PEPTIDE{i + 1}'] = simple_polymer
    
    # Now we look for the connections between the simple polymers
    for connection_pair in connection_pairs:
        tmp_connections = []
    
        edge = G.edges[connection_pair]
    
        # First we look for connections in CHEMICAL
        for pid, simple_polymer in complex_polymer.items():
            if 'CHEM' in pid:
                for k, node in enumerate(connection_pair):
                    if node.split(':')[1] == simple_polymer:
                        tmp_connections.append((pid, 1, edge['attachment_type'][k][1:]))
    
        # Then we look for the missing connection in the rest of the PEPTIDE
        for i, simple_polymer_path in enumerate(simple_polymer_paths):
            simple_polymer_path = simple_polymer_path
    
            for j, node in enumerate(connection_pair):
                if node in simple_polymer_path:
                    tmp_connections.append((f'PEPTIDE{i + 1}', simple_polymer_path.index(node) + 1, edge['attachment_type'][j][1:]))
    
        connections.append((tmp_connections[0][0], tmp_connections[1][0], 
                            tmp_connections[0][1], tmp_connections[0][2], 
                            tmp_connections[1][1], tmp_connections[1][2]))
    
    connections = np.asarray(connections, dtype=dtype)

    return complex_polymer, connections


def MolToHELM(mols, HELM_extra_library_filename=None):
    """
    Generate a list of HELM strings from RDKit molecules

    Parameters
    ----------
    mols : rdkit.Chem.rdchem.Mol or list of rdkit.Chem.rdchem.Mol
        The molecules to convert in HELM string.
    HELM_extra_library_filename : str, default : None
        The path to a HELM Library file containing extra monomers.
        Extra monomers will be added to the internal monomers library. 
        Internal monomers can be overriden by providing a monomer with
        the same MonomerID.

    Returns
    -------
    List
        A list of str.

    """
    helm_strings = []

    if not isinstance(mols, (list, tuple, np.ndarray)):
            mols = [mols]

    # Read HELM Core Library
    d = path_module("mobius")
    HELMCoreLibrary_filename = os.path.join(d, "data/chembl_32_monomer_library.json")

    with open(HELMCoreLibrary_filename) as f:
        data = json.load(f)

    # Re-organize monomer data in a dictionary for faster access
    HELMCoreLibrary = {monomer['MonomerID']: monomer for monomer in data}

    # Read HELM Extra Library
    if HELM_extra_library_filename is not None:
        with open(HELM_extra_library_filename) as f:
            data = json.load(f)

        HELMExtraLibrary = {monomer['MonomerID']: monomer for monomer in data}
        # Add new monomers to the internal monomers library or
        # override existing monomers with the same MonomerID
        HELMCoreLibrary.update(HELMExtraLibrary)

    # Prepare the monomer library
    RDLogger.DisableLog('rdApp.warning')
    prepared_library = prepare_monomer_library(HELMCoreLibrary)
    RDLogger.EnableLog('rdApp.warning')

    for mol in mols:
        # Map monomers to molecule
        monomer_vectors, monomer_infos = map_monomers_to_molecule(mol, prepared_library)

        # Remove all the monomers that are not continuous
        monomer_vectors, monomer_infos = remove_non_continuous_monomers(monomer_vectors, monomer_infos)

        # Now remove all the monomers that are enclosed in bigger monomers
        monomer_vectors, monomer_infos = remove_enclosed_monomers(monomer_vectors, monomer_infos)

        # Check if we fully covered the molecules already with the identified monomers
        # If not, generate the fragments, linkers, etc...
        if not is_peptide_fully_covered(monomer_vectors):
            missing_atom_ids = check_missing_coverage(monomer_vectors)

            fragment_vectors, fragment_infos = map_fragments_to_molecule(mol, missing_atom_ids, monomer_infos)

            # Add them to the rest, they are now proper monomers
            monomer_vectors = np.concatenate([monomer_vectors, fragment_vectors])
            monomer_infos.update(fragment_infos)

        # Build graph
        G = build_graph(mol, monomer_infos)

        # Find all non canonical bonds (monomers NOT connected by R2 -> R1)
        non_canonical_bonds = find_all_non_canonical_bonds(G)
        
        # Find all the linear structures (monomers connected by R2 -> R1), ignoring non canonical bonds (cyclic)
        canonical_structures = find_all_canonical_structures(G, non_canonical_bonds)
        
        # Find all the head to tail structures, ignoring non canonical bonds (cyclic)
        head_to_tail_structures, head_to_tail_bonds = find_all_head_to_tail_structures_and_bonds(G, non_canonical_bonds)
        
        all_bonds = non_canonical_bonds + head_to_tail_bonds
        all_structures = canonical_structures + head_to_tail_structures
        all_structures.sort(key=len, reverse=True)

        complex_polymer, connections = build_complex_polymer_and_connections(G, all_structures, all_bonds, monomer_infos)

        # Build HELM string
        helm_string = build_helm_string(complex_polymer, connections=connections)
        helm_strings.append(helm_string)

    return helm_strings
